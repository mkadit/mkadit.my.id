import { fetch } from '$lib/utils/fetch';

export async function get() {
	const sortedPosts = Object.keys(fetch).map((key) => fetch[key]);
	sortedPosts.sort((b, a) => {
		const da = new Date(a.date).getTime();
		const db = new Date(b.date).getTime();
		if (da < db) return -1;
		if (da === db) return 0;
		if (da > db) return 1;
	});

	return {
		body: { posts: sortedPosts }
	};
}
