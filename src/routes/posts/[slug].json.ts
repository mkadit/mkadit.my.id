import { fetch } from '$lib/utils/fetch';

export async function get(req) {
	const { slug } = req.params;

	const sortedPosts = Object.keys(fetch).map((key) => fetch[key]);
	const post = sortedPosts.find((post) => post.slug === slug);

	return {
		body: { post }
	};
}
