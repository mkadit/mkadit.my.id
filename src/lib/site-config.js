export const siteName = 'MK Adit Journal';

export const authorName = 'MK Adit';

export const website = `https://mkadit.my.id`;

export const description = `My website containing my portofolio, my blogs, and my projects`;

export const pages = [
	{ title: `Home`, path: `/` },
	{ title: `Posts`, path: `/posts` },
	{ title: `About`, path: `/about` }
];

export const socialLinks = [
	{ title: `Twitter`, path: `https://twitter.com/username` },
	{ title: `YouTube`, path: `https://youtube.com/username` },
	{ title: `GitHub`, path: `https://github.com/mkadit` },
	{ title: `GitLab`, path: `https://gitlab.com/mkadit` }
];
