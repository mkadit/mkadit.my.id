---
title: Recap of Bangkit Capstone Project
date: 2022-06-24
published: true
desc: A recap of the cloud architecture for my Bangkit Capstone Project
---

# About the Application

For our capstone project we are tasked to create a chatbot that is
responsible to answer user's question as well as recommending either a
restaurant or a hotel should the user ask for it.

There were few requirements for our project:

- We would require at least one Private API
- We were not given any API or datasets so we would need to create our own
  datasets
- We were not allowed to use automatically machine learning such as the
  machine learning in dialogflow

With that in mind we our chatbot may not be the most sophisticated.

# Cloud Architecture

## Google Cloud Services Used

Our application use several Google Cloud's services:

- Firebase
  This is because with the usage of Firebase we skip the part where we
  painstakingly configure authentication and database. All we need to do is
  just press a few clicks in [Firebase](https://console.firebase.google.com/)
  to configure the authentication method, add app for Android, and lastly
  import the necessary keys to connect to our firebase such as it's web API
  key and it's secret key.
- Cloud Run
  We decided to use Cloud Run because we decided to have our application
  divided into microservices, one for our API server while the other for our
  Machine Learning model, both to be build to Docker images for our Cloud Run
  to just run it's image.
- Container Registry
  Cloud Run would need a place to get the images which is where the Container
  Registry comes in. It's a service that allows us to store our microservices
  in order to be used by Cloud Run.
- Cloud Build
  Mainly used for CI/CD. Pushing and deploying revisions for our Cloud Run
  could be cumbersome, which is where Cloud Build comes in. It's a service
  that executes our builds on Google Cloud from importing source code,
  building an image, pushing it to the Container Registry, as well as
  deploying a revision for code in Cloud Run, all being carry out just by
  setting a trigger for it such as when a particular branch is being pushed.
- Secret Manager
  Our API microservices require several sensitive data such as the web-api
  as well as the secret key to our Firebase. While it may be true that we
  can configure such environment variable in the Cloud Run we it's best to
  be precarious about it, not to mention our secret-key is in the form of a
  file rather than value. Because of that we use Secret Manager to contain
  our sensitive data, having our web-api in value while our secret-key in
  file by mounting them, a feature that Secret Manager is capable of.

Below is a picture of the cloud architecture for our project.

![Capstone Company Project Cloud Architecture](/assets/posts/recap-bangkit-capstone-project/capstone-cloud-architecture.png)

## Application Flow

As seen in the image our client is capable of sending request to both the API
server and the Firebase database (Firestore) directly. The reason why we
didn't centralized it through the API server is because the cloud SDK for
go (the language our API server use) does not support snapshots. Further more
after further thoughts we decided it would be best to let the client device
to handle it directly in order to reduce requests that may come to the
server, reducing the cost for our project.

The message sent to the client will first be sent to the API for processing.
This involves checking if the user is logged in, or to check for a tag in
the message to determined if it's a normal message, a request for a hotel,
or for a restaurant. It also handles the creation of a collection for
storing the user data as well as creating groups to separate each user's
conversation with the BOT, both of which are called from the client.

Should the request be message then the API server would send it through an
API of the ML server, which path it takes is determined by the content of
the tag. Should it be `restaurant_recommendation` then it would be sent to
the restaurant recommendation API, if it's `hot_recommendation` then the
hotel one, and if it's neither of those it will be send to the normal route
which is located it the root all the while the message being sent was sent
to Firestore. Afterwards the ML server would return the message along with a
tag of which the message would be posted to the Firestore while the message
as well as it's request be sent back to the client.

## Development Flow

Cloud Run depends on is a service that require's an image in order for the
application to be run. User would require to push an image of their
application to the Container Registry where after said image would then be
used by Cloud Run. This can be done locally by using the Google Cloud SDK,
building the image according to the URL and pushing it to the Container
Registry with the command `gcloud builds submit` but this method comes at a
cost where you would need to have a person responsible for building the
image locally, and pushing it to the Container Registry. It's tedious, and
inhibit development. Because of that our team chose to use Cloud Build to
automate all of that.

By choosing a repository to be watch as well as it's branch we can have
Cloud Build to do a series of action every time a push was made to said
branch. The first step would be building the image, followed by pushing the
image to the Container Registry and finally have it deployed in Cloud Run.

### Cloud Build Script

```yaml
steps:
      - name: gcr.io/cloud-builders/docker
        args:
          - build
          - '--no-cache'
          - '-t'
          - >-
            $_GCR_HOSTNAME/$PROJECT_ID/github.com/$REPO_NAME/$_SERVICE_NAME:$COMMIT_SHA
          - .
          - '-f'
          - Dockerfile
        id: Build
      - name: gcr.io/cloud-builders/docker
        args:
          - push
          - >-
            $_GCR_HOSTNAME/$PROJECT_ID/github.com/$REPO_NAME/$_SERVICE_NAME:$COMMIT_SHA
        id: Push
      - name: 'gcr.io/google.com/cloudsdktool/cloud-sdk:slim'
        args:
          - run
          - services
          - update
          - $_SERVICE_NAME
          - '--platform=managed'
          - >-
            --image=$_GCR_HOSTNAME/$PROJECT_ID/github.com/$REPO_NAME/$_SERVICE_NAME:$COMMIT_SHA
          - >-
            --labels=managed-by=gcp-cloud-build-deploy-cloud-run,commit-sha=$COMMIT_SHA,gcb-build-id=$BUILD_ID,gcb-trigger-id=$_TRIGGER_ID,$_LABELS
          - '--region=$_DEPLOY_REGION'
          - '--quiet'
        id: Deploy
        entrypoint: gcloud
    images:
      - '$_GCR_HOSTNAME/$PROJECT_ID/github.com/$REPO_NAME/$_SERVICE_NAME:$COMMIT_SHA'
    options:
      substitutionOption: ALLOW_LOOSE
    substitutions:
      _TRIGGER_ID: 59684190-d25c-4f8d-b33a-f042450d1b69
      _GCR_HOSTNAME: gcr.io
      _PLATFORM: managed
      _SERVICE_NAME: chat-bot
      _DEPLOY_REGION: asia-southeast2
      _LABELS: gcb-trigger-id=59684190-d25c-4f8d-b33a-f042450d1b69
    tags:
      - gcp-cloud-build-deploy-cloud-run
      - gcp-cloud-build-deploy-cloud-run-managed
      - chat-bot

```

The above code is what is inside my `.yml` file which configured the steps
Cloud Build would need to take every time changes are made in the branch
it's watching in. You can see that there are 3 steps, each are labelled with
their id namely Build, Push, and Deploy. I won't be going into detail of
what happened here, you can just take a look at it and understand it on your
own.

# API Server Application

Aside from building, pushing, and deploying the application (as well as
creating the CI/CD for them) another task of the CC division is the creation
of the API server. The API server would be responsible for authentication,
and the creation of users, groups as well as posting messages should the
request have the proper credentials along with the given authorizations.

## Project Structure

```
├── Dockerfile
  ├── cmd
  │   └── chat-bot
  ├── docs
  │   ├── docs.go
  │   ├── swagger.json
  │   └── swagger.yaml
  ├── .env
  ├── firebase_key.json
  ├── go.mod
  ├── go.sum
  ├── internal
  │   ├── firebase
  │   └── logger
  ├── pkg
  │   ├── api
  │   └── chats
```

Above is the API server file project. As you can see it's divided into
several folders as along with several files on the root folder.

### cmd

The `cmd` directory is the directory containing the directories/files that will
serve as an executable when building the application. Think of it as the
directory that will contain the main files for the application to built on.

### internal

The `internal` directory is the directory that contain private application and
library code for our application, things that you don't want others
importing in their application or libraries.

### pkg

The `pkg` directory is the directory that contains code that is OK to be
used by external library or application. It's expected that the code in
these directory is expected to work.

### docs

The `docs` directory is the directory containing the documents of the
application design for the user to read. In this particular project we use
`github.com/swaggo/swag` package to automatically generate the documents
for the endpoints that was annotated.

### go.mod

The `go.mod` file is the root of dependency management for the project. All
of the modules that was used for the project are all listed and maintained
in the `go.mod` file. Sort of like `requirements.txt` for python or
`package.json` for javascript.

### go.sum

The `go.sum` file is a file that maintains the checksum so that when the
project is run again it will not install all the packages again but instead
use the ones on cache. Automatically generated, and is not recommended to
modify this file.

### Dockerfile

The `Dockerfile` is a file that contains a series of steps to build the
application. Things like installing packages, building the executable,
exposing ports, and run it.

### .env and firebase_key.json

They're files that will be used in the application. The former containing
series of variables of which our application will use while the latter is
the private key for our firebase server. I just listed them here to clarify
the things we have in our project but when in production we excluded the
two files when building the image and instead directly handle them in the
Cloud Run and by using Secret Manager service on Google Cloud. This is to
avoid any untoward people messing around with our application.

## Initialization

I could tell you step by step but I think that will just waste our times so
I'm just going to give you the link to the
[code](https://github.com/C22-CB01/chat-bot-server/tree/main) and tell you a
few things to get things starts up.
I'm assuming you already initialize your project in which case it's time to
start getting the packages.

#### Dependencies

- [fiber](https://github.com/gofiber/fiber) for the web framework
- [firebase-sdk-go](https://firebase.google.com/docs/firestore/quickstart)
- [godotenv](https://github.com/joho/godotenv) for reading environment variable
- [swag](https://github.com/swaggo/swag) for automatically creating API endpoints documentation
- [swagger](https://github.com/gofiber/swagger) for swag middleware for fiber
- [zap](https://github.com/uber-go/zap) for logging when developing

#### What to do?

1. Creating the main file for the application, this can be found on
   [/cmd/chat-bot/main.go](https://github.com/C22-CB01/chat-bot-server/blob/main/cmd/chat-bot/main.go)
   where you will initialize everything, starting from loading the
   environment variables, setting the logger, connecting to the firebase
   (and with it the firebase auth as well as the firestore) before creating
   the server and run it.
2. Creating the middleware of logger and swag for documentation of API
   endpoints which can be found on
   [/internal/logger/logger.go](https://github.com/C22-CB01/chat-bot-server/blob/main/internal/logger/logger.go)
3. Configured the server, from setting up the swag, setting up the router,
   handling the router for the application as well as setting references to
   the app that was initialized on step 1 which can be found on
   [/pkg/api/server.go](https://github.com/C22-CB01/chat-bot-server/blob/main/pkg/api/server.go)
4. Configure the routes such as setting up the middleware, and the API
   endpoints which can be found on
   [/pkg/api/routes.go](https://github.com/C22-CB01/chat-bot-server/blob/main/pkg/api/routes.go)
5. Create the service that will run the logic and the handler that will
   determined the return responses (the thing that will be called in the
   routes) which can be found on
   [/pkg/chats](https://github.com/C22-CB01/chat-bot-server/tree/main/pkg/chats)

## Firebase Setup

The setting up of firebase can be sum up into two (at least for this
project), the initialization part, and the creation of middleware. The
former is already done in [What to do?](#what-to-do) so the only thing left is to
configured the middleware that will be responsbile to determined if a
request is allowed or not.

#### Logic of Authentication

1. The request's Authorization header will be checked. If there's a value
   to Authorization and if it's determined that it's a Bearer token then
   extract the token if not return false.
   e.x: `Authorization: Bearer <TOKEN>`
2. The extracted token will be checked and verify as seen on
   [/internal/firebase/firebase.go](https://github.com/C22-CB01/chat-bot-server/blob/main/internal/firebase/firebase.go)
   if the token is not usable then return false.
3. Store the token claims on local an continue to the next method in stack
   that match the current route.
   With the middleware done all you need to do is to apply it to the API
   routes you want it to be applied on (check
   [/pkg/api/routes.go](https://github.com/C22-CB01/chat-bot-server/blob/main/pkg/api/routes.go))

## Setting up API Documentation

We use `github.com/swaggo/swag` to automatically create our documentation
based on annotations. Those `@` things that can be found on various files
such as
[/cmd/chat-bot/main.go](https://github.com/C22-CB01/chat-bot-server/blob/main/cmd/chat-bot/main.go)
and
[/pkg/chats/handler.go](https://github.com/C22-CB01/chat-bot-server/tree/main/pkg/chats/handler.go).
For complete documentation on what does what check out
[swaggo/swag](https://github.com/swaggo/swag)

# Deploying Application

There are many ways we could deploy the application, Compute Engine, App
Engine, Google Kubernetes Engine (GKE), etc. But the way our team decided to deploy
it is through Cloud Run this is mainly because:

- We didn't have any database dependence with the exception of Firestore (of
  which we can directly set up and access through the SDK)
- We want it to be it's deployment to be easy while at the same time we want
  it to be scalable. The former is the main reason we chose it over GKE since
  managing Kubernetes cluster could be quite difficult without the proper
  training, which we are since we're not exposed to it as much we want it to
  be.
- Integration with environment variables as well as credentials is simple.
  The former can be set directly in Cloud Run instance while the latter use
  just need the addition of Secret Manager.
  As for deployment all I did was follow the
  [guide](https://cloud.google.com/run/docs/quickstarts) and deploy it by
  choosing the github repository. But just with this we would need to manually
  rebuild the application over and over again which is where Cloud Build comes
  in. Just go to Cloud Build set the triggers and put input script as the
  ones I have shown in [Cloud Build Script](#cloud-build-script).

# Possible development for the future

As great as the work done in our project it's not perfect yet. There are
several things that could be done to make the project better such as:

- Changing connections between microservices to gRPC to make
  communication between them faster.
- Have unit tests in order to create a stable working application.
- Have the model used for the Machine Learning server be stored in
  somewhere else and not in the code base.
