/** @type {import('tailwindcss').Config} */

module.exports = {
	content: ['src/**/*.svelte'],
	theme: {
		extend: {
			colors: {
				nord_night: {
					100: '#2e3440',
					200: '#3b4252',
					300: '#434c5e',
					400: '#4c566a'
				},
				nord_storm: {
					100: '#d8dee9',
					200: '#e5e9f0',
					300: '#eceff4'
				},
				nord_frost: {
					100: '#8fbcbb',
					200: '#88c0d0',
					300: '#81a1c1',
					400: '#5e81ac'
				},
				nord_aurora: {
					100: '#bf616a',
					200: '#d08770',
					300: '#ebcb8b',
					400: '#a3be8c',
					500: '#b48ead'
				}
			}
		}
	},
	plugins: [require('@tailwindcss/typography'), require('daisyui')],
	daisyui: {
		themes: [
			'dracula',
			'aqua',
			'black',
			'bumblebee',
			'corporate',
			'cupcake',
			'cyberpunk',
			'dark',
			'emerald',
			'fantasy',
			'forest',
			'garden',
			'halloween',
			'light',
			'lofi',
			'luxury',
			'pastel',
			'retro',
			'synthwave',
			'valentine',
			'wireframe',
			{
				nord: {
					primary: '#8FBCBB' /* Primary color */,
					'primary-focus': '#88C0D0' /* Primary color - focused */,
					'primary-content': '#D8DEE9' /* Foreground content color to use on primary color */,

					secondary: '#81A1C1' /* Secondary color */,
					'secondary-focus': '#5E81AC' /* Secondary color - focused */,
					'secondary-content': '#D8DEE9' /* Foreground content color to use on secondary color */,

					accent: '#434C5E' /* Accent color */,
					'accent-focus': '#4C566A' /* Accent color - focused */,
					'accent-content': '#B48EAD' /* Foreground content color to use on accent color */,

					neutral: '#2E3440' /* Neutral color */,
					'neutral-focus': '#3B4252' /* Neutral color - focused */,
					'neutral-content': '#D8DEE9' /* Foreground content color to use on neutral color */,

					'base-100': '#ECEFF4' /* Base color of page, used for blank backgrounds */,
					'base-200': '#E5E9F0' /* Base color, a little darker */,
					'base-300': '#D8DEE9' /* Base color, even more darker */,
					'base-content': '#2E3440' /* Foreground content color to use on base color */,

					info: '#EBCB8B' /* Info */,
					success: '#A3BE8C' /* Success */,
					warning: '#D08770' /* Warning */,
					error: '#BF616A' /* Error */
				}
			}
		]
	}
};
